﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public class Account
    {

        public const int CHECKING = 0;
        public const int SAVINGS = 1;
        public const int MAXI_SAVINGS = 2;

        private readonly int accountType;
        public List<Transaction> transactions;

        public Account(int accountType)
        {
            this.accountType = accountType;
            this.transactions = new List<Transaction>();
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
            }
        }

        public double InterestEarned()
        {
            double amount = SumTransactions();
            return InterestEarnedForAccountType(amount);
            
        }

        public double SumTransactions()
        {
            return CheckIfTransactionsExist(true);
        }

        private double CheckIfTransactionsExist(bool checkAll)
        {
            double amount = 0.0;
            foreach (Transaction t in transactions)
                amount += t.amount;
            return amount;
        }

        public int GetAccountType()
        {
            return accountType;
        }

        // Code modified by Abi
        // modified code

        public void AmountTransfer(Account ToAccount, double amount)
        {
            if (ToAccount != null)
            {
                if ((amount > 0) && (this.SumTransactions()> amount))
                { 
                    this.Withdraw(amount);
                    ToAccount.Deposit(amount);
                }

            }
        }

        private bool CheckIfWithdrawTransactionsExist(int days)
        {
            foreach (Transaction t in transactions)
            {
                if (t.transactionDate >= DateTime.Now.AddDays(-days) && t.amount<0) 
                {
                    return true;
                }
            }
            return false;
        }

        public double InterestCalculationForDays()
        {
            double amount = 0.0;
            var currentDate = DateTime.Now;
            var Date = currentDate.Date;
            foreach (Transaction t in transactions)
            {

                if (t.transactionDate.Date == DateTime.Now.Date)
                {
                    amount += t.amount;
                }

            }
            this.Deposit( InterestEarnedForAccountType(amount));
            return 0.00;
        }

        public double InterestEarnedForAccountType(double amount)
        { 
            
                switch (accountType)
                {
                    case SAVINGS:
                        if (amount <= 1000)
                            return amount * 0.001;
                        else
                            return 1 + (amount - 1000) * 0.002;
                    
                    case MAXI_SAVINGS:
                        if (!CheckIfWithdrawTransactionsExist(10))
                            return amount * 0.05;
                        else
                            return amount * 0.001;

                    default:
                        return amount * 0.001;
                }
            }
            
        
    }
}
